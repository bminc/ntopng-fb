# ntopng
Provides [ntopng GUI](https://www.ntop.org/products/traffic-analysis/ntop/) with connection to FritzBox capture stream to analyze all WAN traffic in real-time.

## Build
Currently fails with shared GitLab Runner. Needs to build on a RPi locally. Nevertheless the the manual pushed image can be used as below.

## Run
```
docker run -d --name=ntopng -e FRITZPWD=<PWD-of-fritz-box> -p 3000:3000 registry.gitlab.com/bminc/ntopng-fb
```
### Other parameters

| Parameter | Description | Default |
| --------- | ----------- | ------- |
| `FRITZIP` | IP (v4) of FritzBox | 192.168.178.1 |
| `IFACE` | FritzBox interface (WAN: 2-0, LAN: 1-lan)| 2-0 |
| `FRITZUSER` | FritzBox user; If you use password-only authentication use 'dslf-config' as username.| dslf-config |
| `FRITZPWD` | FritzBox password; Mandatory| |

Parameters to ntopng can be set as docker image arguments.
